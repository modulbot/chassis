# The Robot's Chassis

## Description
![Photo of the printed part](RoboChassis.jpeg "A photo of the chassis to explain what this is all about..")

For a project with kids at Greentech.Berlin<sup>[1](#1)</sup>,
we are building a modular autonomously driving robot.
The first subproject is an intelligent rolling chassis,
 controlled by an ESP32<sup>[2](#2)</sup>.

This repository is only storing the 3D-design of the chassis.
We created a separate repository for the wheels<sup>[3](#3)</sup>.

![Serving suggestion - photo of a chassis with continous track made from recycled bicycle tube](KidzRoboChassisContinousTrack.jpg "A photo of the different forms of wheels, the rounded edge can be used with continous tracks. Sort of a serving suggestion that is.")

## Installation

The two parts of the chassis were crafted with
FreeCAD<sup>[4](#4)</sup> to hold the electric
motors and the rest of the robot.

The back part of the chassis is expecting

If needed you can open and edit the `.FCStd` file with FreeCAD.

In case you do not want to edit the diameters you can use the `.stl`
file, import it into your favorite slicer, and eventually print the wheel with
your 3D-printer of joice (tested with PrusaSlicer<sup>[5](#5)</sup> on a MK3S+<sup>[6](#6)</sup>).

After printing, just push the motors over the axle and tighten the clamp with an
M3 screw (DIN 912) and nut.

To get more friction on a smooth surface you can cut a small slice of an old
bicycle hose and slip it over the wheel.


## Usage

We use the wheels to drive a rolling chassis for a modular robot project.
The details will also be published as soon as it is ready.

The wheel has 16 equally distributed holes around its center.
These holes can be used to monitor the
rotation of the wheel.

To only build a simple vehicle you can basically mount two e-motors onto
a stick, with a second stick (or e.g. some wire) used as a stabilizer "wheel".

Depending on the polarization of the current, the vehicle can drive forwards,
backwards, or turn around left and clock- or counterclockwise.

## Support / Contributing

For any questions or suggestions, please directly contact us (i.e. the authors, see below).

## Roadmap

This repository is responsible only for the wheels, so there are no big improvements planned here.
Future developments on the chassis or the modular robot itself will be made in separate repositories
respectively.

## Authors and acknowledgment

  * Michael Lustenberger: mic\_at\_greentech.berlin

## License

The work here is published unter the terms of the
"CERN Open Hardware Licence Version 2 - Permissive"<sup>[7](#7)</sup> (for details please see the LICENSE file).

## Project status

Stable and in use...

---

  1. <a name="1" href="https://greentech.berlin">https://greentech.berlin</a>
  1. <a name="2" href="https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/index.html">https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/index.html</a> (last access: 2022-07-23)
  1. <a name="3" href="https://gitlab.com/modulbot/wheels">https://gitlab.com/modulbot/wheels</a> (last access: 2023-04-16)
  1. <a name="4" href="https://freecadweb.org">https://freecadweb.org</a> (last access: 2022-07-23)
  1. <a name="5" href="https://www.prusa3d.com/page/prusaslicer_424/">https://www.prusa3d.com/page/prusaslicer_424/</a> (last access: 2022-07-23)
  1. <a name="6" href="https://www.prusa3d.com/product/original-prusa-i3-mk3s-3d-printer-3/">https://www.prusa3d.com/product/original-prusa-i3-mk3s-3d-printer-3/)</a> (last access: 2022-07-23)
  1. <a name="7" href="https://choosealicense.com/licenses/cern-ohl-p-2.0/">https://choosealicense.com/licenses/cern-ohl-p-2.0/</a> (last access: 2022-07-23)
